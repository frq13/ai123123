from flask import Flask, request
import json
import re
import long_responses as long
import os
from flask_cors import CORS
import re

app = Flask(__name__)
CORS(app)  # Enable CORS for all routes


def message_probability(user_message, recognised_words, single_response=False, required_words=[]):
    message_certainty = 0
    has_required_words = True


    # Counts how many words are present in each predefined message
    for word in user_message:
        if word in recognised_words:
            message_certainty += 1

    # Calculates the percent of recognised words in a user message
    percentage = float(message_certainty) / float(len(recognised_words))

    # Checks that the required words are in the string
    for word in required_words:
        if word not in user_message:
            has_required_words = False
            break

    # Must either have the required words, or be a single response
    if has_required_words or single_response:
        return int(percentage * 100)
    else:
        return 0


lorem1000 = "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis autem accusantium exercitationem amet debitis minima, porro illum ad saepe cupiditate. Voluptatum quisquam ad pariatur placeat, ea maxime veniam omnis deserunt, quos doloremque asperiores fugiat dolore aperiam, soluta eligendi. Non quis tempore sequi esse eius doloremque blanditiis commodi voluptates, excepturi cumque! Quidem cupiditate rerum harum nesciunt sapiente vel tenetur architecto sed ea nisi corrupti eveniet, unde culpa sit tempore dolor laudantium perferendis blanditiis molestiae sunt repudiandae repellat laborum animi. Veritatis quidem vitae sit sapiente cum reiciendis minus commodi delectus consectetur quibusdam ipsam debitis, officia nisi, fuga magnam saepe iusto ducimus quis at similique! Deserunt voluptate quidem nihil repudiandae esse earum recusandae provident sapiente distinctio, exercitationem quae magni nostrum minus accusantium modi maxime, sunt, aliquid molestias itaque magnam ipsum quos? Excepturi nam nisi ratione culpa natus voluptatem. Repellendus, ab autem quibusdam distinctio corrupti optio earum sint velit, iure nam nulla animi libero enim error placeat itaque non dignissimos. Sunt nostrum suscipit obcaecati? Voluptatum, rerum. Fuga veritatis aut sequi beatae dolorum ipsa quidem necessitatibus dolore, blanditiis perferendis nulla exercitationem tenetur eum repudiandae! Impedit dignissimos animi voluptates veniam veritatis qui natus similique! Quibusdam magni est sequi porro, rerum, harum possimus cumque fugit maxime eligendi nam omnis distinctio repellendus mollitia excepturi quasi eos, similique aliquid odio repudiandae? Velit quis nobis non dicta a quibusdam dolor ex totam nemo doloribus ullam necessitatibus, officiis dolorum adipisci sed, iure molestiae quas consequuntur. At architecto quas similique et amet adipisci exercitationem accusantium nisi! Corrupti dolorem debitis temporibus ipsam esse? Repellendus fugiat iusto quod dicta, similique reprehenderit nulla mollitia animi labore consequuntur eveniet nostrum eius! Modi numquam earum incidunt inventore quod magni sint. Provident eligendi tempore, consequatur labore expedita dolore ut dignissimos, ad, eius aliquid officiis accusamus aspernatur! Odio, nobis error aut exercitationem atque saepe accusantium sit, adipisci deleniti ea aliquid sunt neque. Distinctio autem iure aspernatur earum nulla maxime ut consectetur odio inventore harum fuga dicta, et nihil enim ad velit, officia ducimus laborum. Atque ipsum distinctio non obcaecati amet perspiciatis laudantium illum, tenetur architecto quae explicabo. Sequi accusantium, autem excepturi ducimus minima pariatur voluptates inventore, exercitationem cupiditate assumenda, iusto numquam. Quibusdam veritatis suscipit officiis, sapiente fuga quasi. Minima eius facilis obcaecati cum expedita laudantium voluptatibus maxime cumque delectus repellat quam ipsa quibusdam magnam, eligendi, voluptatum ad inventore ut repellendus officiis? Reiciendis voluptas quas culpa unde nam, praesentium excepturi pariatur distinctio, nostrum voluptates mollitia eligendi, omnis laudantium nesciunt ipsam amet eos laborum aliquam asperiores soluta consequatur illo. Et officia, ullam enim magni laudantium corrupti nostrum, optio aut modi facere doloremque, debitis dolores. Non, repellat. Repellendus officiis atque quasi corrupti amet ea autem expedita culpa et fugit! Fuga ipsam numquam beatae necessitatibus dolor illo. Sapiente, magni iusto. Voluptate voluptatibus assumenda tempora facilis neque odit voluptates culpa, voluptatem quaerat hic placeat iusto provident ipsum impedit id, non quia numquam iure ipsam praesentium deserunt dolorum perspiciatis. Necessitatibus tempora aspernatur dolor ea sequi et, sapiente autem, quia suscipit repudiandae id vel alias incidunt exercitationem nobis. Officia dolorem laudantium, eveniet et pariatur asperiores, omnis nisi, eius adipisci inventore maiores. Ipsum nostrum, aliquam placeat nesciunt quibusdam cupiditate ab sapiente quos error! Pariatur, blanditiis. Architecto, rem possimus? Doloremque ea delectus culpa natus autem veniam, aut neque quisquam ut optio tempora expedita doloribus odio consequatur iste illum fugit laboriosam dolores atque eaque odit ex temporibus excepturi. Laudantium autem sit in repudiandae tempora error cum reprehenderit dicta. Unde harum molestiae, magnam perferendis autem beatae distinctio recusandae quos voluptas cum eligendi atque ad? Accusantium facere eligendi minima excepturi officiis quae inventore ex, ipsum temporibus recusandae. Atque consectetur asperiores repellendus ipsam recusandae, earum architecto natus aperiam quidem harum veritatis optio, repudiandae illo quibusdam id et commodi placeat eaque temporibus laborum. Similique molestias velit vel, excepturi veritatis quidem? Recusandae, unde! Quis illo quae ullam doloremque quaerat eaque dolorum ipsa nesciunt, libero tempore debitis in voluptatibus hic harum. Doloremque ducimus temporibus exercitationem, debitis tempore quaerat quisquam ea quam expedita in fuga a? Illo molestias repudiandae reprehenderit labore quisquam beatae enim rem. Totam tempore velit dignissimos! Ex voluptatibus ratione dolore deserunt incidunt exercitationem minus id vel sequi, quae soluta impedit unde non quia quisquam explicabo reprehenderit quo voluptate! Vitae vero non, quod accusantium odit culpa modi eligendi, tempore, molestias magnam numquam reprehenderit alias rerum dignissimos expedita? Laudantium, quaerat hic saepe voluptatum rerum adipisci cupiditate quidem aut officiis libero sit dignissimos nulla neque minima? Error illo dolorem rem alias. Voluptates, neque inventore aut quos ullam eligendi possimus reiciendis nobis facilis quas! Beatae eveniet nulla nihil praesentium, nesciunt rem? Error quaerat quo voluptate quasi asperiores reiciendis enim illo quidem assumenda harum nulla accusantium unde cumque nobis mollitia ratione atque quam porro pariatur blanditiis, id velit! Doloribus repellat at voluptate, eos rem perspiciatis sequi sit quos, mollitia odit maxime impedit beatae? Odio officia distinctio iure dolor? Non, nisi. Inventore officia qui dolorem quaerat corporis velit reprehenderit, laudantium porro est quasi hic quidem assumenda, fugiat odit repellendus nobis, numquam ea molestiae! Excepturi nemo reiciendis quisquam officia, aliquam repudiandae provident. Ipsam nisi perspiciatis consequatur. Blanditiis autem quisquam delectus dolores voluptatibus ipsum tempore soluta ipsa temporibus pariatur, assumenda iure modi qui neque iste nulla, consectetur dicta cumque quod, voluptatem ab minus rem. Quidem asperiores, perferendis optio aperiam quod molestiae adipisci. Quae reiciendis dicta, quidem autem perferendis blanditiis reprehenderit, aliquam officiis provident magnam a! Aliquid itaque corrupti quis fugit, eveniet modi quaerat distinctio incidunt labore esse facilis eaque voluptatem maxime recusandae odit, nesciunt repellendus ipsam delectus excepturi molestiae, dolorum laboriosam et repudiandae repellat. Amet, recusandae unde. Tenetur, adipisci. Ad natus delectus odio amet minus sed iste sequi, accusantium quas laborum. Culpa commodi sint nam quidem blanditiis nemo ullam similique officia mollitia sapiente expedita dolores ipsum ad, voluptate reprehenderit laudantium. Distinctio vitae itaque animi nam suscipit. Accusamus consequatur voluptatem dolorem quaerat natus fugit, excepturi veritatis numquam non amet quia. Molestiae, sequi voluptatum perspiciatis aspernatur asperiores magni velit consequuntur neque in harum incidunt accusantium eaque repellendus quisquam omnis esse nisi similique. Nobis consequatur explicabo iure velit sed eaque ex cupiditate voluptatum distinctio ducimus. Ab numquam provident aperiam rem maiores ea ut eius."

words = lorem1000.split()
first_10_words = ' '.join(words[:10])

def check_all_messages(message):
    highest_prob_list = {}

    # Simplifies response creation / adds it to the dict
    def response(bot_response, list_of_words, single_response=False, required_words=[]):
        nonlocal highest_prob_list
        highest_prob_list[bot_response] = message_probability(message, list_of_words, single_response, required_words)

    # Responses -------------------------------------------------------------------------------------------------------
    response('Haii', ['hello', 'hi', 'hey', 'sup', 'hai', 'heyo'], single_response=True)
    response('See you!', ['bye', 'goodbye'], single_response=True)
    response('I\'m doing fine, and you?', ['how', 'are', 'you', 'doing'], required_words=['how'])
    response('You\'re welcome!', ['thank', 'thanks'], single_response=True)
    response('Thank you!', ['i', 'love', 'code', 'palace'], required_words=['code', 'palace'])
    response('I\'m a Artificial Intellegence Models, created by fariq', ['who', 'are', 'you'], required_words=['who', 'you'])
    response('yea sure', ['really?'], required_words=['really'])
    response('my creator', ['who', 'is', 'fariq', 'it', 'him'], required_words=['who', 'fariq'])
    response('He is live in small town near Surabaya, East Java', ['where', 'live', 'fariq', 'is'], required_words=['where', 'fariq', 'live'])
    response('She is my Creator Lover <3', ['who', 'kai', 'is'], required_words=['who', 'kai', 'is'])
    response('yea, he did', ['did', 'fariq', 'your', 'creator', 'have', 'lover', 'partners'], required_words=['did', 'lover'])
    response('her name is kai', ['who', 'is', 'her', 'name'], required_words=['her', 'name'])
    response('Abseloutly!, my creator would be sad if she doesn\'t', ['do', 'kai', 'love', 'loves', 'your', 'creator'], required_words=['creator', 'kai', 'love'])
    response('i wish both of them always doing great <3', ['do', 'you', 'have', 'wish', 'wishes', 'for', 'both', 'them', 'words'], required_words=[ 'wish', 'both'])

    # Longer responses
    response(long.R_ADVICE, ['give', 'advice'], required_words=['advice'])
    response(long.R_EATING, ['what', 'you', 'eat'], required_words=['you', 'eat'])
    best_match = max(highest_prob_list, key=highest_prob_list.get)
    # print(highest_prob_list)
    # print(f'Best match = {best_match} | Score: {highest_prob_list[best_match]}')

    return long.unknown() if highest_prob_list[best_match] < 1 else best_match


# Used to get the response
def get_response(user_input):
    split_message = re.split(r'\s+|[,;?!.-]\s*', user_input.lower())
    response = check_all_messages(split_message)
    return response
    



app = Flask(__name__)

@app.route('/api/ai', methods=['GET'])
def ai_endpoint():
    # Retrieve the user input from the URL parameter
    user_input = request.args.get('text')

    # Check if the user input is 'hai'
    resp = get_response(user_input)
    response_headers = {
        'Access-Control-Allow-Origin': '*',  # Set the allowed origins for CORS
        'Referrer-Policy': 'no-referrer-when-downgrade'  # Set the Referrer Policy
    }
    # Return the response as an HTTP response
    print(user_input)
    return resp, 200, response_headers

if __name__ == '__main__':
    app.run()
